#define MENU_EXTEND "extend"
#define MENU_DONTCHANGE "dontchange"

public Action Command_RTV(int client, int args)
{
    if (!gB_Initialized)
    {
        ReplyToCommand(client, "MapChooser plugin has not finished initializing");
        return Plugin_Handled;
    }
    ToggleRTV(client);
    return Plugin_Handled;
}

public Action Command_Revote(int client, int args)
{
    if (!gB_RTVstarted) return;
    gM_RTVMenu.Display(client, 30);
}

void ToggleRTV(int client)
{
    gB_RTV[client] = !gB_RTV[client];
    // Can do or remove rtv from client
    if (gB_RTV[client])
    {
        gI_RTVcount += 1;
        // Check RTV ratio
        PrintToChatAll("%N has RTV", client);
        CheckRTV();
    }
    else
    {
        gI_RTVcount -= 1;
        PrintToChatAll("%N has rescinded their RTV", client);
    }
}

void CheckRTV()
{
    if (IsVoteInProgress()) return;
    if (gB_RTVstarted) return;
    int playerCount = GetClientCount(true);
    float ratio = gI_RTVcount / float(playerCount);
    if (ratio - gF_RTVratio > 0.0)
    {
        gB_RTVstarted = true;
        InitializeRTVMenu();
        gM_RTVMenu.DisplayVoteToAll(30);
    }
}

void InitializeRTVMenu()
{
    Menu menu = new Menu(Menu_RTVHandler);
    menu.SetTitle("RTV");
    menu.VoteResultCallback = RTV_Callback;
    menu.Pagination = MENU_NO_PAGINATION;

    menu.AddItem("line_1", "asdf", ITEMDRAW_DISABLED);
    menu.AddItem("line_2", "lol cant see this", ITEMDRAW_DISABLED);

    for (int i = 0; i < MAX_DIFFICULTY; i++)
    {
        ArrayList maps = tieredMaps[i];
        int randomIndex = GetRandomInt(0, maps.Length - 1);
        char map[64];
        maps.GetString(randomIndex, map, sizeof(map));
        menu.AddItem(map, map);
    }

    menu.AddItem(MENU_EXTEND, "Extend");
    menu.AddItem(MENU_DONTCHANGE, "Don't Change");

    gM_RTVMenu = menu;
}

int Menu_RTVHandler(Menu menu, MenuAction action, int client, int item)
{
    if (action == MenuAction_End)
    {
        // delete menu;
    }
}

void RTV_Callback(Menu menu, int num_votes, int num_clients, const int[][] client_info, int num_items, const int[][] item_info)
{
    gB_RTVstarted = false;

    int winner = 0;
    int numTies = WinnerCount(num_items, item_info);
    if (numTies == 0)
    {
        winner = item_info[0][VOTEINFO_ITEM_INDEX];
    }
    else
    {
        winner = GenerateWinner(menu.ItemCount, item_info, numTies);
    }

    if (winner == -1)
    {
        // do nothing
    }

    PrintToServer("%i winner", winner)

    // Check vote index winner
    if (num_clients == 0)
    {
        ExtendTime(15);
    }

    char map[64];
    menu.GetItem(item_info[winner][VOTEINFO_ITEM_INDEX], map, sizeof(map));
    PrintToChatAll("Map %s was selected by RTV", map);
    // ServerCommand("changelevel %s", map);
    delete menu;
}

int WinnerCount(int num_items, const int[][] item_info)
{
    int ties = 0;
    int max = item_info[0][VOTEINFO_ITEM_VOTES];
    for (int i = 1; i < num_items; i++)
    {
        if (item_info[i][VOTEINFO_ITEM_VOTES] == max)
            ties++;
        else
            break;
    }
    return ties;
}

int GenerateWinner(int menuCount, const int[][] item_info, int numTies)
{
    // Check for extend/dont change
    for (int i = 0; i < numTies; i++)
    {
        if (menuCount - 1 == item_info[i][VOTEINFO_ITEM_INDEX])
        {
            // Dont Change
            return -1;
        }
        if (menuCount - 2 == item_info[i][VOTEINFO_ITEM_INDEX])
        {
            ExtendTime(15);
            return -1;
        }
    }
    return GetRandomInt(0, numTies);
}