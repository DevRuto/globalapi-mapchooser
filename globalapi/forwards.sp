static Handle H_OnMapNominated;

void CreateForwards()
{
    H_OnMapNominated = CreateGlobalForward("GlobalAPI_OnMapNominated", ET_Ignore, Param_Cell, Param_String);
}

void Call_OnMapNominated(int client, const char[] map)
{
    Call_StartForward(H_OnMapNominated);
    Call_PushCell(client);
    Call_PushString(map);
    Call_Finish();
}