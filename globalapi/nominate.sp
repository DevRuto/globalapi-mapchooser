public Action Command_Nominate(int client, int args)
{
    if (!gB_Initialized)
    {
        ReplyToCommand(client, "MapChooser plugin has not finished initializing");
        return Plugin_Handled;
    }
    DisplayTierMenu(client);
    return Plugin_Handled;
}

static char difficulties[][] =
{
    "Very Easy",
    "Easy",
    "Medium",
    "Hard",
    "Very Hard",
    "Death"
};

void DisplayTierMenu(int client)
{
    Menu menu = new Menu(Menu_TierHandler);
    menu.SetTitle("Nominate");

    for (int i = 0; i < sizeof(difficulties); i++)
    {
        menu.AddItem(difficulties[i], difficulties[i]);
    }

    menu.Display(client, MENU_TIME_FOREVER);
}

int Menu_TierHandler(Menu menu, MenuAction action, int client, int item)
{
    if (action == MenuAction_End)
    {
        delete menu;
    }
    if (action == MenuAction_Select)
    {
        ReplyToCommand(client, "index: %i", item);
        DisplayNominateMenu(client, item);
    }
}

void DisplayNominateMenu(int client, int tier)
{
    Menu menu = new Menu(Menu_NominateHandler);
    char title[25];
    FormatEx(title, sizeof(title), "Nominate - %s", difficulties[tier]);
    menu.SetTitle(title);

    ArrayList maps = tieredMaps[tier];
    for (int i = 0; i < maps.Length; i++)
    {
        char map[64];
        maps.GetString(i, map, sizeof(map));
        menu.AddItem(map, map);
    }

    menu.Display(client, MENU_TIME_FOREVER);
}

int Menu_NominateHandler(Menu menu, MenuAction action, int client, int item)
{
    if (action == MenuAction_End)
    {
        delete menu;
    }

    if (action == MenuAction_Select)
    {
        char map[64];
        menu.GetItem(item, map, sizeof(map));
        PrintToChatAll("%N nominated %s", client, map);
        Call_OnMapNominated(client, map);
    }
}