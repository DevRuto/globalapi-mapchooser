void LoadMapList()
{
    char[] url = "https://kztimerglobal.com/api/v1.0/maps?is_validated=true";
    Handle handle = SteamWorks_CreateHTTPRequest(k_EHTTPMethodGET, url);
    SteamWorks_SetHTTPRequestUserAgentInfo(handle, "globalapi-mapchooser");
    SteamWorks_SetHTTPCallbacks(handle, API_OnCompleted, API_OnHeaders, API_OnDataReceived);
    SteamWorks_SendHTTPRequest(handle);
}

public int API_OnCompleted(Handle hRequest, bool bFailure, bool bRequestSuccessful, EHTTPStatusCode eStatusCode)
{
    if (bFailure || eStatusCode != k_EHTTPStatusCode200OK)
    {
        SetFailState("Unable to retrieve map list");
        delete hRequest;
        return;
    }
}

public int API_OnHeaders(Handle hRequest, bool bFailure)
{

}

public int API_OnDataReceived(Handle hRequest, bool bFailure, int offset, int bytesreceived)
{
    if (bFailure)
    {
        SetFailState("Unable to retrieve map list");
        delete hRequest;
        return;
    }
    else
    {
        int responseSize = 0;
        SteamWorks_GetHTTPResponseBodySize(hRequest, responseSize);
        if (responseSize > 0)
        {
            SteamWorks_GetHTTPResponseBodyCallback(hRequest, API_OnData);
        }
        else
        {
            SetFailState("Unable to retrieve map list");
        }
    }

    delete hRequest;
}

public int API_OnData(const char[] response)
{
    JSON_Array apiMaps = view_as<JSON_Array>(json_decode(response));
    PrintToServer("%i maps retrieved from api", apiMaps.Length);

    for (int i = 0; i < apiMaps.Length; i++)
    {
        JSON_Object map = apiMaps.GetObject(i);
        // Double check max map length
        char mapName[64];
        if (map.GetString("name", mapName, sizeof(mapName)))
        {
            if (!StrEqual(gC_CurrentMap, mapName, false) && localMapList.FindString(mapName) > -1)
            {
                allMaps.PushString(mapName);
                tieredMaps[map.GetInt("difficulty")-1].PushString(mapName);
            }
        }
    }
    PrintToServer("Map chooser initialized");

    json_cleanup(apiMaps);
    gB_Initialized = true;
}