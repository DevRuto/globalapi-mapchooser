# TODO

## Nominate
- [ ] First menu shows All maps option and tiers
    - [ ] Display menu based on choice
- [ ] Selected Item broadcasts nominate in chat
    `%N has nominated %s`
- [ ]


## RTV
- [ ] First 2 options are blanked out to prevent command misclick from Checkpoint/Teleport binds
    - [ ] Empty 3rd slot?
- [ ] Give one tier of each map in RTV
- [ ] Nominating map of X difficulty will replace X slot in RTV menu of corresponding tier
    - [ ] Prioritize First nominator, to prevent last second nomination abuse

## Fancy stuff

- [ ] Fuzzy search?
- [ ] Privileged votes? (VIP worth 2 votes or something)