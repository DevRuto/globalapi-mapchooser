#define PLUGIN_NAME "GlobalAPI - Map Chooser"
#define PLUGIN_AUTHOR "Ruto"
#define MAX_DIFFICULTY 6

#pragma dynamic 131072

#include <sourcemod>

#include <SteamWorks>
#include <json>

ArrayList localMapList  = null;

ArrayList allMaps       = null;
ArrayList veryEasyMaps  = null;
ArrayList easyMaps      = null;
ArrayList mediumMaps    = null;
ArrayList hardMaps      = null;
ArrayList veryHardMaps  = null;
ArrayList deathMaps     = null;
ArrayList tieredMaps[6];

bool gB_Initialized = false;
char gC_CurrentMap[64];

bool gB_RTVstarted = false;
bool gB_RTV[MAXPLAYERS + 1] = false;
float gF_RTVratio = 0.6;
int gI_RTVcount = 0;

Menu gM_RTVMenu = null;

ConVar gCV_TimeLimit;

#include "globalapi/api.sp"
#include "globalapi/rtv.sp"
#include "globalapi/nominate.sp"
#include "globalapi/commands.sp"
#include "globalapi/forwards.sp"

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = "Map chooser based on the global map pool",
	version = "0.01",
	url = ""
};

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
    RegPluginLibrary(PLUGIN_NAME);
    CreateCommands();
    CreateForwards();
}

public void OnPluginStart()
{
    gCV_TimeLimit = FindConVar("mp_timelimit");
}

public void OnMapStart()
{
    int blockSize   = ByteCountToCells(PLATFORM_MAX_PATH);
    allMaps         = new ArrayList(blockSize);
    localMapList    = new ArrayList(blockSize);
    easyMaps        = new ArrayList(blockSize);
    veryEasyMaps    = new ArrayList(blockSize);
    mediumMaps      = new ArrayList(blockSize);
    hardMaps        = new ArrayList(blockSize);
    veryHardMaps    = new ArrayList(blockSize);
    deathMaps       = new ArrayList(blockSize);
    tieredMaps[0] = easyMaps;
    tieredMaps[1] = veryEasyMaps;
    tieredMaps[2] = mediumMaps;
    tieredMaps[3] = hardMaps;
    tieredMaps[4] = veryHardMaps;
    tieredMaps[5] = deathMaps;
    
    int serial = -1;
    ReadMapList(localMapList, serial, "default", MAPLIST_FLAG_CLEARARRAY);
    gB_Initialized = false;
    GetCurrentMap(gC_CurrentMap, sizeof(gC_CurrentMap));
    GetMapDisplayName(gC_CurrentMap, gC_CurrentMap, sizeof(gC_CurrentMap));
    LoadMapList();
}

public void OnMapEnd()
{
    allMaps.Clear();
    for (int i = 0; i < MAX_DIFFICULTY; i++)
    {
        tieredMaps[i].Clear();
    }
    localMapList.Clear();
    CloseHandle(gM_RTVMenu);
}

public void OnClientPostAdminCheck(int client)
{

}

public void OnClientDisconnect(int client)
{

}

// Stuff
void ExtendTime(int time)
{
    gCV_TimeLimit.SetInt(gCV_TimeLimit.IntValue + time, false, true);
}